# RNSIT FINAL YEAR PROJECT 2020
## PROJECT TOPIC : "VIoLET: A Large-scale Virtual Environment for Internet of Things"

* Team members:
    1. Shankar Anbalagan
    1. Shwetha KM

VIoLET is a simulator for large scale IoT systems

## Project Phase 1
### Tasks completed for Phase 1
1. Understanding and learning containers and docker
1. Implement virtual sensors and create sensor images
1. Create a mock UI to generate deployment documents
1. Create deployment manager to run virtual sensors as 

Click [here](https://drive.google.com/open?id=1wbAG0hVo_LONnvxiIH9Tu3e8y7PpCQ7E) to see project phase 1 report
