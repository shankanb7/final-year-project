import os
import sys
import json
from datetime import datetime

deploy_docs=json.load(open(r'./UI/deploy_docs.json'))
devices=deploy_docs['devices']
network=deploy_docs['network']
sensors=deploy_docs['sensors']
log_file=open('deploy_logs.txt','a')
timeobj=datetime.now()
time = timeobj.strftime("%d-%b-%Y (%H:%M:%S.%f)")

print("Starting deployment manager.....")
log_file.write(time+" : Starting deployment manager\n")
print("Loading docker images.....")
log_file.write(time+" : Loading docker images\n")
print("Reading deployment documents.....")
log_file.write(time+" : Reading deployment documents\n")
print("Creating devices.....")
log_file.write(time+" : Creating devices\n")
print("Done\n")
log_file.write(time+" : Done\n")
print("Device List:\n")
log_file.write("\nDevice List:\n")


for sensor in sensors:
    sensor_type=sensor['type']
    port=sensor['port']
    print("Device name: {}".format(sensor['name']))
    log_file.write("Device name: {}\n".format(sensor['name']))
    print("Running on port no.: {}\n".format(sensor['port']))
    log_file.write("Running on port no.: {}\n".format(sensor['port']))
    print("Device container ID:")
    id=os.system("docker run -p {0}:5000 -d shankanb/sensor_{1}".format(port,sensor_type))
    log_file.write("Device container ID: {}\n".format(id))

print("All devices created")
log_file.close()