from flask import Flask, render_template, request, redirect, flash
import json

app=Flask(__name__)
app.secret_key="yo"
jsonDict = {"devices":[], "sensors":[], "network":[]}

@app.route('/', methods=['GET', 'POST'])
def index():
	if request.method == 'POST':
		if request.form['btn'] == "ADD Device":
			devList = jsonDict["devices"]
			devDict  = {
				"name": request.form.get("dname"), 
				"type": request.form.get("dtype"), 
				"port": request.form.get("dport"),
				"ip": request.form.get("dip")
			}
			devList.append(devDict)

		if request.form['btn'] == "ADD Sensor":
			senList = jsonDict["sensors"]
			senDict  = {
				"name": request.form.get("sname"), 
				"type": request.form.get("stype"), 
				"port": request.form.get("sport"), 
			}
			senList.append(senDict)

		if request.form['btn'] == "ADD Network":
			ntwList = jsonDict["network"]
			ntwDict  = {
				"name": request.form.get("nname"), 
				"bandwidth-mbps": request.form.get("bandwidth"),
				"latency-ms": request.form.get("latency"),
			}
			ntwList.append(ntwDict)

		if request.form['btn'] == "DONE":
			#print(jsonDict)
			with open('deploy_docs.json', 'w') as json_file:
				json.dump(jsonDict, json_file)
			
	return render_template('index.html')


if __name__ == '__main__':
	app.run(host='localhost', port=5000, debug=True)
