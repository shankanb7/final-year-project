import os
from datetime import datetime

log_file=open('deploy_logs.txt','a')
timeobj=datetime.now()
time = timeobj.strftime("%d-%b-%Y (%H:%M:%S.%f)")

print ("stopping all running containers........")
os.system("docker stop $(docker ps -a -q)")
print ("stopped all running containers")
log_file.write("\n{} : All running containers stopped\n".format(time))
log_file.close()
