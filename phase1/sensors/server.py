import sys
from flask import Flask
import numpy
import json
import math

app=Flask(__name__)
sensor_type="default"
sensor_data=json.load(open("sensor_types.json"))['sensor_types']

@app.route('/')
def func():
    if sensor_type=='gyroscope':
        for sensor_def in sensor_data:
            if sensor_def['type']=='gyroscope':
                lam=float(sensor_def['value_params']['lambda'])
                val=numpy.random.poisson(lam=lam)
                break
    elif sensor_type=='accelerometer':
        for sensor_def in sensor_data:
            if sensor_def['type']=='accelerometer':
                mean=float(sensor_def['value_params']['mean'])
                var=float(sensor_def['value_params']['variance'])
                sd=math.sqrt(var)
                val=numpy.random.normal(loc=mean,scale=sd)
                break
    else:
        val=numpy.random.rand()
    
    return str(val)


if __name__=="__main__":
    args=sys.argv
    try:
        sensor_type=args[1]
    except:
        sensor_type="default"
    app.run(host='0.0.0.0')